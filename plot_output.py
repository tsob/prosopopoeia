"""
@name: plot_output.py
@desc: Plot training output from the Prosopopoeia net
@auth: Tim O'Brien
@date: Fall 2016
"""


import sys
import os
import argparse
import logging
import pickle
import itertools
from distutils.dir_util import mkpath
import pprint
import datetime as dt
from dateutil.parser import parse as parse_date
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns


sns.set_style("darkgrid")
sns.set_palette("dark")


def plot_output(stats, output_dir):
    """
    """

    palette = itertools.cycle(sns.color_palette())

    # Parse file name for run
    statsfile = os.path.basename(stats["filename"])
    times = statsfile.split('_')[:6]
    datestr = "{}-{}-{} {}:{}:{}".format(*times)
    datestr = dt.datetime.strftime(parse_date(datestr),
                                   '%A, %B %d, %Y %I:%M:%S %p')

    fig = plt.figure(figsize=(14, 8))
    gs = gridspec.GridSpec(8, 14)

    # Plot loss curves

    ax_loss = plt.subplot(gs[0:3, :8])

    if 'loss' in stats.keys():
        # Plot training loss
        col = next(palette)
        ax_loss.plot(stats['loss'], color=col, label="Train loss")
        ax_loss.set_ylabel("Training Loss", color=col)
        # Make the y-axis label and tick labels match the line color.
        for tl in ax_loss.get_yticklabels():
            tl.set_color(col)
    if 'val_loss' in stats.keys():
        col = next(palette)
        ax_val_loss = ax_loss.twinx()
        ax_val_loss.plot(stats['val_loss'], color=col, label="Validation loss")
        ax_val_loss.set_ylabel("Validation Loss", color=col)
        ax_val_loss.grid(False)
        # Make the y-axis label and tick labels match the line color.
        for tl in ax_val_loss.get_yticklabels():
            tl.set_color(col)
    plt.title(datestr)

    # Plot learning rate

    plt.subplot(gs[3:5, :8], sharex=ax_loss)
    if 'lr' in stats.keys():
        # Plot training accuracy
        plt.plot(stats['learning_rate'], label="Learning rate")
        plt.xlabel("Epoch")
        plt.ylabel("Learning rate")
        plt.ylim([0.9*min(stats['learning_rate']),
                  1.111*max(stats['learning_rate'])
                  ])

    # Plot accuracy curves

    plt.subplot(gs[5:8, :8], sharex=ax_loss)
    if 'acc' in stats.keys():
        # Plot training accuracy
        plt.plot(stats['acc'], label="Train accuracy")
    if 'val_acc' in stats.keys():
        plt.plot(stats['val_acc'], label="Validation accuracy")
        plt.xlim([0, len(stats['val_acc'])])

    plt.ylabel("Accuracy")
    plt.xlabel("Epoch")
    plt.legend(loc="best")

    # Add config

    if 'config' in stats.keys():
        ax_config = plt.subplot(gs[0:6, 8:14])
        plt.text(0, 1,
                 "{}\n{}".format(
                     "Config parameters:",
                     pprint.pformat(stats["config"], width=30, indent=2)
                    ),
                 fontsize=10,
                 verticalalignment='top'
                 )
        ax_config.axis('off')

    fig.set_tight_layout(True)

    # Pickle matplotlib figure
    filepath = os.path.join(
                    output_dir,
                    "plots",
                    "{}.pkl".format(os.path.splitext(statsfile)[0]),
                    )
    mkpath(os.path.dirname(filepath))
    with open(filepath, "wb") as fh:
        pickle.dump(fig, fh)
    logger.info("Saved fig to {}".format(filepath))

    # Save fig image
    filepath = os.path.join(
                    output_dir,
                    "plots",
                    "{}.pdf".format(os.path.splitext(statsfile)[0]),
                    )
    mkpath(os.path.dirname(filepath))
    plt.savefig(filepath)
    logger.info("Saved fig to {}".format(filepath))


def parse_logging_level(level_arg):
    """Set the logging level based on the verbosity argument.

    Parameters
    ----------
    level_arg : int
        The desired level (0, 1, 2, etc.)

    Returns
    -------
    int
        The level to give to the `logging` module.
    """
    if level_arg == 0:
        logging_level = logging.WARNING
    elif level_arg == 1:
        logging_level = logging.INFO
    elif level_arg == 2:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.DEBUG

    return logging_level


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--stats",
                        required=True,
                        help="Stats pkl file, as output by proso.py")
    parser.add_argument("-o", "--outputdir",
                        required=False,
                        default="outputs",
                        help="Directory to output plots etc.")
    parser.add_argument("-v", "--verbosity",
                        action="count",
                        help="increase output verbosity, e.g. -vv for "
                             + "debug level.")
    args = parser.parse_args()

    # Setup logger
    logging_level = parse_logging_level(args.verbosity)
    logging.basicConfig(
            level=logging_level,
            format='[%(levelname)s] %(message)s',
            )

    # Get logger instance
    logger = logging.getLogger(__name__)
    logger.info("Running {}!".format(os.path.basename(sys.argv[0])))
    logger.info("Loading stats from {}".format(args.stats))

    # Load in our stats
    with open(args.stats, 'rb') as fh:
        stats = pickle.load(fh)
        stats["filename"] = args.stats

    plot_output(stats, args.outputdir)
