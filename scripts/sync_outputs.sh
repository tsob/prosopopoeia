#!/usr/bin/env bash

# cd to directory of this script.
SCRIPT_DIR="$(dirname "$(realpath "$0")")";
OUTPUTS_PATH="$(realpath $SCRIPT_DIR/../outputs)";
cd $OUTPUTS_PATH

if [ -z ${PROSO_REMOTE+x} ];
then
  echo "PROSO_REMOTE environment variable is unset. Set it to the remote you want to use.";
else
  echo "PROSO_REMOTE is set to '$PROSO_REMOTE'";
fi

rsync -arvp tsob@$PROSO_REMOTE:/scratch/tsob/prosopopoeia/outputs/ .
