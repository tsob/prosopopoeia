#!/usr/bin/env bash

desc="Generate a wav file from a given MIDI file."
usage="Usage: $0 wavfile.MID outputfile.wav"

if [ -z ${1+x} ];
then (printf "Input MIDI file is unset.\n\n$0 - $desc\n$usage\n\n") && exit 1;
fi

if [ -z ${2+x} ];
then (printf "Output file path is unset.\n\n$0 - $desc\n$usage\n\n") && exit 1;
fi

# Check that timidity is installed
command -v timidity >/dev/null 2>&1 || {
  echo >&2 "I require timidity but it's not installed. Aborting."; exit 1;
}

timidity --output-24bit -A120 $1 -Ow -o $2
