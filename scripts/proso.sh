#!/bin/bash

# cd to main project directory
SCRIPT_DIR="$(dirname "$(realpath "$0")")";
PROSO_PATH="$(realpath $SCRIPT_DIR/..)";
cd $PROSO_PATH

# Filter out annoying TensorFlow log info messages
# Adapted from Fenugreek's comment at
# https://github.com/tensorflow/tensorflow/issues/566

python proso.py $* 3>&1 1>&2 2>&3 3>&- | grep -v ^I\ | grep -v ^pciBusID | grep -v ^major: | grep -v ^name: |grep -v ^Total\ memory:|grep -v ^Free\ memory:
