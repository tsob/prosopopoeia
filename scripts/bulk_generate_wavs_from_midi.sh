#!/usr/bin/env bash

desc="Generate wav files by recursively searching a directory for MIDI files."
usage="Usage: $0 directory/to/search/ output/directory/"

if [ -z ${1+x} ];
then (printf "Directory is unset.\n\n$0 - $desc\n$usage\n\n") && exit 1;
fi

if [ -z ${2+x} ];
then (printf "Output directory is unset.\n\n$0 - $desc\n$usage\n\n") && exit 1;
fi

# Check that timidity is installed
command -v timidity >/dev/null 2>&1 || {
  echo >&2 "I require timidity but it's not installed. Aborting."; exit 1;
}

# Make the output directory if it doesn't exist
if [[ ! -e ${2%/} ]];
then
  mkdir  -p $2
elif [[ ! -d ${2%/} ]];
then
  echo "$2 already exists but is not a directory" 1>&2 && exit 1;
fi

# Render each MIDI file to wav
for f in $(find `readlink -f $1` -iname [^._]*.mid);
do
  # Get the path for the output file
  path="$(readlink -f $2)/$(basename ${f%.[Mm][Ii][Dd]}.wav)";

  # Render with timidity
  timidity --output-24bit -A120 "$f" -Ow -o "$path";
done
