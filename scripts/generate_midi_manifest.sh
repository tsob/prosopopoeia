#!/usr/bin/env bash

desc="Generate a txt list of midi files by recursively finding them in a dir."
usage="Usage: $0 directory/to/search/ [outputfile.txt]"

if [ -z ${1+x} ];
then (printf "Directory is unset.\n\n$0 - $desc\n$usage\n\n") && exit 1;
fi

if [ -z ${2+x} ];
  then find `readlink -f $1` -iname [^._]*.mid;
  else echo "Searching directory $1 and outputting to $2." && \
    (find `readlink -f $1` -iname [^._]*.mid > $2);
fi
