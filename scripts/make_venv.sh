#!/usr/bin/env bash
# Create a virtual environment for this project. Linux-only.

# cd to directory of this script.
SCRIPT_DIR="$(dirname "$(realpath "$0")")";
PROSO_PATH="$(realpath $SCRIPT_DIR/..)";
cd $PROSO_PATH

VENV_DIRECTORY=venv
GPU_STR=$(lspci | grep NVIDIA)


if [ ! -d "$VENV_DIRECTORY" ];
then
  # Control will enter here if $VENV_DIRECTORY doesn't exist.
  virtualenv $VENV_DIRECTORY
  ./add_scripts_to_venv_path.sh $VENV_DIRECTORY
  source ./$VENV_DIRECTORY/bin/activate
  pip install --upgrade pip

  # Choose whether to install GPU or CPU TensorFlow
  if [ -z ${GPU_STR} ];
  then
    echo "Didn't detect GPU. Using CPU version of TensorFlow.";
    export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.11.0-cp27-none-linux_x86_64.whl
  else
    echo "Detected GPU. Installing GPU version of TensorFlow r0.10.";
    # export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-0.11.0-cp27-none-linux_x86_64.whl
    export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-0.10.0-cp27-none-linux_x86_64.whl

    # Prepend environment variables to the activate script
    ACTIVATE_PATH=$VENV_DIRECTORY/bin/activate
    cp $ACTIVATE_PATH $ACTIVATE_PATH.bkup  # Back it up just in case
    PATH_ADDITION_TEXT="export CUDA_HOME=/usr/local/cuda && export CUDNN_HOME=/scratch/tsob/cuDNN/cuda && export PATH=\$CUDA_HOME/bin:\$CUDNN_HOME/include:\$PATH && export CPATH=\$CUDNN_HOME/include:\$CPATH && export LD_LIBRARY_PATH=\$CUDNN_HOME/lib64:\$CUDNN_HOME/include:\$CUDA_HOME/lib64:\$LD_LIBRARY_PATH"
    echo $PATH_ADDITION_TEXT | cat - $ACTIVATE_PATH > /tmp/out && mv /tmp/out $ACTIVATE_PATH

  fi

  # Install Tensorflow
  pip install --upgrade $TF_BINARY_URL
  # Install the rest
  pip install --upgrade Keras
  pip install -r requirements.txt

  echo "New virtual environment is available at $VENV_DIRECTORY."
  echo "Activate it with $VENV_DIRECTORY/bin/activate"

fi
