# Prosopopoeia

A neural net to transform [MIDI][midi-wiki] scores into expressive MIDI performances. As the name [Prosopopoeia][proso-def] implies, our goal is to automatically add expressive, performative, human touches to a musical score expressed in [MIDI format][midi-spec].

[proso-def]: http://www.merriam-webster.com/dictionary/prosopopoeia
[midi-wiki]: https://en.wikipedia.org/wiki/MIDI
[midi-spec]: https://www.midi.org/specifications

## TO DO:

- See [issues](https://cm-gitlab.stanford.edu/tsob/prosopopoeia/issues).

## Getting Started

These instructions will get you a copy of the project up and running on your
machine.

### Data

In order to train and test Prosopopoeia, you'll have to download lots of
performance MIDI data and quantize a copy of each back to a "flat score" state.

* TODO - instructions about how to download the MIDI files.

### Prerequisities / Installing

You'll likely want to create a Python virtual environment for this project. We
use Python 2.7.12, but likely any 2.7.* version will do. We also use
`virtualenv` version 15.0.1, but a reasonably current version should work.

Once you have Python and `virtualenv`, you can execute the following from the
main project directory:

```bash
$ ./scripts/make_venv.sh
```

This will download and setup the virtual environment in `./venv/`. From then
on, you can activate the virtual environment by executing:

```bash
$ source ./venv/bin/activate
```


## Running the tests

*TODO:* Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Authors

* **Tim O'Brien** - *Initial work* - [tsob](https://github.com/tsob)

* **Julius O. Smith III** - *Ideas and strokes of genius*

## License

This project is licensed under the MIT License - see the
[LICENSE](LICENSE) file for details.

## Acknowledgments

* To be added
