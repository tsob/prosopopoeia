#!/usr/bin/env bash

desc="Make your Python virtual environment add the scripts folder to your PATH"
usage="Usage: $0 virtualEnvFolder/"

# Check if we received the venv as a commandline arg
if [ -z ${1+x} ];
then
  (printf "Virtual Environment Folder is unset.\n\n$0 - $desc\n$usage\n\n") && exit 1;
fi

# Check if it exists, and is a directory
if [[ ! -e ${1%/} ]];
then
  echo "$1 doesn't exist." 1>&2 && exit 1;
elif [[ ! -d ${1%/} ]];
then
  echo "$1 already exists but is not a directory" 1>&2 && exit 1;
fi

# Path to activation script, which we'll augment
activation_path="$(readlink -f $1)/bin/activate";
if [[ ! -e ${1%/} ]];
then
  echo "Activation script $activation_path doesn't exist." 1>&2 && exit 1;
fi

# Prepend the new path to the virtualenv activation script
cp $activation_path ${activation_path}.bkup
echo "export PATH==\$PATH:$(readlink -f scripts)"|cat - $activation_path > /tmp/out && mv /tmp/out $activation_path
