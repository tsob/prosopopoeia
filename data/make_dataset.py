"""
@name: make_dataset.py
@desc: Make a Keras-ready dataset from a list of midi files.
@auth: Tim O'Brien
@date: October 13, 2016
"""

# Avoid integer division errors...
from __future__ import division

# General stuff
import os
import copy
import numpy as np
import pickle
from distutils.dir_util import mkpath

# Logging and argument parsing
import logging
import argparse

# Multiprocessing so this doesn't take forever
from multiprocessing import Pool

# Parse MIDI files via the midi module
import midi

# Use NamedTuples for some parameters
from collections import namedtuple

# Global Parameters -----------------------------------------------------------
# Encode each MIDI message as [Type, Tick, and Data x 2]
MAX_MIDI_MESSAGE_LENGTH = 4

# Codes for the MIDI message encoding
MidiMessageType = namedtuple(
        'MidiMessageType',
        ['NOTEON',                 # Note-related
         'NOTEOFF',                # Note-related
         'AFTERTOUCH',             # Note-related
         'CONTROLCHANGE',          # Pedal-related (in Disklavier case)
         'START_OF_TRACK',         # Track-related
         'END_OF_TRACK',           # Track-related
         'TRACK_NAME_EVENT',       # Metadata we don't feed to the net
         'INSTRUMENT_NAME_EVENT',  # Metadata we don't feed to the net
         'SET_TEMPO_EVENT',        # Metadata we don't feed to the net
         'TIME_SIGNATURE_EVENT',   # Metadata we don't feed to the net
         'TEXT_META_EVENT',        # Metadata we don't feed to the net
         'IGNORE']
        )
MIDI_MESSAGE_CODES = MidiMessageType(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, -1)

# When quantizing pressures, just use these values for all note[on, off] events
QUANTIZED_NOTEON_PRESSURE = 127
QUANTIZED_NOTEOFF_PRESSURE = 64

# Tick quantum, in quarter notes, for aligning performance events with a beat
# grid.
QUANTIZED_BEAT_SUBDIVISION = 1/24.0

# Fraction of set to allocate to training/validation;
# the rest is reserved for test.
TRAIN_FRACTION = 0.8

# Maximum length of each sequence, in MIDI notes, per song/performance.
MAX_SEQUENCE_LENGTH = 100


def get_file_list_from_manifest(manifest_filename=None):
    """Given a path to a manifest file, return a list of its contents.

    Parameters
    ----------
    manifest_filename : str
        The path to the manifest file.

    Returns
    -------
    list
        The paths for each individual midi file.
    """
    logger = logging.getLogger(__name__)
    logger.info("Getting files from {}".format(manifest_filename))

    manifest_filename = os.path.abspath(manifest_filename)
    if not os.path.isfile(manifest_filename):
        logger.error("Could not find input file {}".format(manifest_filename))
        return None

    with open(manifest_filename, 'r') as fh:
        paths = fh.read().splitlines()

    logger.info("Found {} files.".format(len(paths)))

    return paths


def parse_logging_level(level_arg):
    """Set the logging level based on the verbosity argument.

    Parameters
    ----------
    level_arg : int
        The desired level (0, 1, 2, etc.)

    Returns
    -------
    int
        The level to give to the `logging` module.
    """
    if level_arg == 0:
        logging_level = logging.WARNING
    elif level_arg == 1:
        logging_level = logging.INFO
    elif level_arg == 2:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.DEBUG

    return logging_level


def parse_midi_pattern(midi_file):
    """Given a MIDI pattern, parse its contents.

    Parameters
    ----------
    midi_file : str
        The path to a MIDI file.

    Returns
    ----------
    parsed_midi_pattern : numpy.ndarray
        The Numpy array representing the MIDI pattern, suitable for input
        to the net as training targets.

    ticks_per_quarter : float
        The temporal resolution of the midi file.
    """
    # Get logger
    logger = logging.getLogger(__name__)

    # Read MIDI file
    midi_pattern = midi.read_midifile(midi_file)

    # How many ticks per quarter note?
    ticks_per_quarter = midi_pattern.resolution
    logger.debug("Resolution is {} ticks per quarter note.".format(
        ticks_per_quarter))

    midi_pattern = midi_pattern[0]  # Assume 1 track

    # Get the number of examples we'll derive from this file
    n_examples = int(np.ceil(len(midi_pattern)/float(MAX_SEQUENCE_LENGTH)))

    parsed_midi_pattern = np.zeros(
            (n_examples, MAX_SEQUENCE_LENGTH, MAX_MIDI_MESSAGE_LENGTH),
            dtype=np.int64)

    # Instantiate a MIDI pattern stub to keep around. It doesn't go through
    # the net, but we keep it around for reconstructing MIDI files.
    midi_stub = {}
    midi_stub["resolution"] = ticks_per_quarter
    midi_stub["track"] = []

    i_example = 0  # Current chunk of the MIDI file?
    i_evt = 0      # This sequence's event index

    # Now loop over all messages in the track
    for msg in midi_pattern:

        # Get the message type (as int)
        msg_type = parse_midi_message_type(msg)

        # Parse message if it's not one of the types we ignore
        if msg_type != MIDI_MESSAGE_CODES.IGNORE:
            if msg_type == MIDI_MESSAGE_CODES.TRACK_NAME_EVENT:
                # These are the messages we'll keep around in the midi_stub
                midi_stub["track"].append(dict(
                    msg_type=msg_type,
                    tick=msg.tick,
                    text=msg.text,
                    data=msg.data
                    ))
            elif msg_type == MIDI_MESSAGE_CODES.INSTRUMENT_NAME_EVENT:
                # These are the messages we'll keep around in the midi_stub
                midi_stub["track"].append(dict(
                    msg_type=msg_type,
                    tick=msg.tick,
                    text=msg.text,
                    data=msg.data
                    ))
            elif msg_type == MIDI_MESSAGE_CODES.SET_TEMPO_EVENT:
                # These are the messages we'll keep around in the midi_stub
                midi_stub["track"].append(dict(
                    msg_type=msg_type,
                    tick=msg.tick,
                    data=msg.data
                    ))
            elif msg_type == MIDI_MESSAGE_CODES.TIME_SIGNATURE_EVENT:
                # These are the messages we'll keep around in the midi_stub
                midi_stub["track"].append(dict(
                    msg_type=msg_type,
                    tick=msg.tick,
                    data=msg.data
                    ))
            elif msg_type == MIDI_MESSAGE_CODES.TEXT_META_EVENT:
                # These are the messages we'll keep around in the midi_stub
                midi_stub["track"].append(dict(
                    msg_type=msg_type,
                    tick=msg.tick,
                    text=msg.text,
                    data=msg.data
                    ))
            else:
                # These are the messages we'll parse into our nmp.
                parsed_midi_pattern[i_example, i_evt, 0] = msg_type
                parsed_midi_pattern[i_example, i_evt, 1] = msg.tick

                if msg_type == MIDI_MESSAGE_CODES.END_OF_TRACK:
                    # End of track message has no data. Fill with zeros.
                    parsed_midi_pattern[i_example, i_evt, 2:] = [0, 0]
                else:
                    # Add msg data
                    parsed_midi_pattern[
                        i_example, i_evt, 2:2+len(msg.data)] = msg.data

                i_evt += 1

        if i_evt >= MAX_SEQUENCE_LENGTH:
            i_evt = 0
            i_example += 1

    return parsed_midi_pattern, midi_stub


def quantize_parsed_midi_pattern(input_tuple):
    """Quantize the parsed midi pattern to approximate the score.

    Parameters
    ----------
    input_tuple : tuple
        A tuple containing the following two elements. Note that this is
        necessary in conjunction with multiprocessing, which takes only onei
        argument.
        * parsed_midi_pattern : numpy.ndarray
              The NumPy array corresponding to the parsed MIDI pattern.
        * ticks_per_quarter : float
              The temporal resolution of the midi file.

    Returns
    -------
    numpy.ndarray
        The quantized, parsed MIDI pattern approximating the score. Same shape
        as the input pattern.
    """

    # Parse tuple
    parsed_midi_pattern, ticks_per_quarter = input_tuple

    logger = logging.getLogger(__name__)

    # Make a copy before we quantize
    X = copy.copy(parsed_midi_pattern)

    n_del = 0  # Keep track of rows we delete, so we can zero-pad.

    # Screen out aftertouch
    if np.any([X[:, 0] == MIDI_MESSAGE_CODES.AFTERTOUCH]):
        logger.debug("Screening out aftertouch events")
        # Get indices of rows we'll delete
        idx = np.where(X[:, 0] == MIDI_MESSAGE_CODES.CONTROLCHANGE)[0]
        n_del += len(idx)
        # Loop over idx, add the to-be-deleted message's tick value to the next
        # message.
        for i in idx:
            if i+1 < X.shape[0]:
                X[i+1, 1] = X[i+1, 1] + X[i, 1]
        # Delete the rows
        X = np.delete(X, idx, axis=0)

    # Screen out control changes
    if np.any([X[:, 0] == MIDI_MESSAGE_CODES.CONTROLCHANGE]):
        logger.debug("Screening out control change events")
        # Get indices of rows we'll delete
        idx = np.where(X[:, 0] == MIDI_MESSAGE_CODES.CONTROLCHANGE)[0]
        n_del += len(idx)
        # Loop over idx, add the to-be-deleted message's tick value to the next
        # message.
        for i in idx:
            if i+1 < X.shape[0]:
                X[i+1, 1] = X[i+1, 1] + X[i, 1]
        # Delete the rows
        X = np.delete(X, idx, axis=0)

    # Zero-pad if necessary
    if n_del > 0:
        X = np.vstack((X, np.zeros((n_del, X.shape[1]))))

    # Sanity check
    assert X.shape == parsed_midi_pattern.shape

    # Quantum for time quantization
    quantum = QUANTIZED_BEAT_SUBDIVISION * ticks_per_quarter
    logger.debug("Quantizing to nearest {} ticks.".format(quantum))

    # Quantize the ticks
    ticks = X[:, 1]
    ticks = quantize_array(ticks, quantum, dtype=np.int64)
    X[:, 1] = ticks

    # Quantize the velocities - Note On events
    note_on_events = X[X[:, 0] == MIDI_MESSAGE_CODES.NOTEON]
    note_on_events[:, 3] = QUANTIZED_NOTEON_PRESSURE
    X[X[:, 0] == MIDI_MESSAGE_CODES.NOTEON] = note_on_events

    # Quantize the velocities - Aftertouch events
    aftertouch_events = X[X[:, 0] == MIDI_MESSAGE_CODES.AFTERTOUCH]
    aftertouch_events[:, 3] = QUANTIZED_NOTEON_PRESSURE
    X[X[:, 0] == MIDI_MESSAGE_CODES.AFTERTOUCH] = aftertouch_events

    # Quantize the velocities - Note Off events
    note_off_events = X[X[:, 0] == MIDI_MESSAGE_CODES.NOTEOFF]
    note_off_events[:, 3] = QUANTIZED_NOTEOFF_PRESSURE
    X[X[:, 0] == MIDI_MESSAGE_CODES.NOTEOFF] = note_off_events

    return X


def quantize_array(arr, quantum, dtype=np.int64):
    """Quantize an array to a given step value.

    Parameters
    ----------
    arr
        The array to be quantized.
    quantum : float
        The value by which to quantize each element in the array.

    Returns
    -------
    numpy.ndarray
        The quantized array.
    """
    arr = np.asarray(
            np.around(arr/quantum) * quantum,
            dtype=dtype
            )
    return arr


def quantize_scalar(num, quantum):
    """Quantize a scalar to a given step value.

    Parameters
    ----------
    num
        The number to be quantized.
    quantum : float
        The value by which to quantize the number.

    Returns
    -------
    float
        The quantized number.
    """
    qnum = round(num/quantum) * quantum
    return qnum


def parse_midi_message_type(midi_msg):
    """Return an integer value based on the type of MIDI message.

    Parameters
    ----------
    midi_msg : midi.Event
       The MIDI message to parse.

    Returns
    -------
    int
        The integer corresponding to the MIDI message type.
    """

    msg_type = type(midi_msg)

    if msg_type == midi.NoteOnEvent:
            msg_int = MIDI_MESSAGE_CODES.NOTEON
    elif msg_type == midi.NoteOffEvent:
            msg_int = MIDI_MESSAGE_CODES.NOTEOFF
    elif msg_type == midi.AfterTouchEvent:
            msg_int = MIDI_MESSAGE_CODES.AFTERTOUCH
    elif msg_type == midi.ControlChangeEvent:
        if midi_msg.channel == 0:
            msg_int = MIDI_MESSAGE_CODES.CONTROLCHANGE
        else:
            msg_int = MIDI_MESSAGE_CODES.IGNORE
    elif msg_type == midi.EndOfTrackEvent:
        msg_int = MIDI_MESSAGE_CODES.END_OF_TRACK
    elif msg_type == midi.TrackNameEvent:
        msg_int = MIDI_MESSAGE_CODES.TRACK_NAME_EVENT
    elif msg_type == midi.TextMetaEvent:
        msg_int = MIDI_MESSAGE_CODES.TEXT_META_EVENT
    elif msg_type == midi.InstrumentNameEvent:
        msg_int = MIDI_MESSAGE_CODES.INSTRUMENT_NAME_EVENT
    elif msg_type == midi.SequencerSpecificEvent:
        msg_int = MIDI_MESSAGE_CODES.IGNORE
    elif msg_type == midi.SysexEvent:
        msg_int = MIDI_MESSAGE_CODES.IGNORE
    elif msg_type == midi.TimeSignatureEvent:
        msg_int = MIDI_MESSAGE_CODES.TIME_SIGNATURE_EVENT
    elif msg_type == midi.SetTempoEvent:
        msg_int = MIDI_MESSAGE_CODES.SET_TEMPO_EVENT
    elif msg_type == midi.ProgramChangeEvent:
        msg_int = MIDI_MESSAGE_CODES.IGNORE
    else:
        msg_int = MIDI_MESSAGE_CODES.IGNORE

    return msg_int


def get_n_events_for_file(midi_file):
    """Get the number of events in a MIDI file.

    Parameters
    ----------
    midi_msg : midi.Event
       The MIDI message to parse.

    Returns
    -------
    int
        The integer corresponding to the MIDI message type.
    """
    logger = logging.getLogger(__name__)

    # Load the midi file
    midi_pattern = midi.read_midifile(midi_file)

    # We only expect 1 track, since we only have piano music. Verify:
    n_tracks = len(midi_pattern)
    if n_tracks != 1:
        logger.warning(
            "More than one track in {}. This is not expected.".format(
                midi_file))

    # Keep track of the number of events in each file
    return len(midi_pattern[0])


def write_midi_from_array(array, midi_stub, output_file):
    """
    Given an array in our condensed format, write out a MIDI file.

    TODO finish doc
    """

    # Parse dimensionality
    n_msg, n_feat = array.shape

    # Instantiate the MIDI pattern
    midi_pattern = midi.Pattern(resolution=midi_stub["resolution"])

    # Get the MIDI track to populate; add it to the pattern
    track = midi.Track()
    midi_pattern.append(track)

    # Parse the midi_stub into the beginning of the track
    for msg in midi_stub["track"]:
        msg_type = msg["msg_type"]
        if msg_type == MIDI_MESSAGE_CODES.TRACK_NAME_EVENT:
            midi_event = midi.TrackNameEvent(tick=msg["tick"],
                                             text=msg["text"],
                                             data=msg["data"]
                                             )
            track.append(midi_event)
        elif msg_type == MIDI_MESSAGE_CODES.INSTRUMENT_NAME_EVENT:
            midi_event = midi.InstrumentNameEvent(tick=msg["tick"],
                                                  text=msg["text"],
                                                  data=msg["data"]
                                                  )
            track.append(midi_event)
        elif msg_type == MIDI_MESSAGE_CODES.SET_TEMPO_EVENT:
            midi_event = midi.SetTempoEvent(tick=msg["tick"],
                                            data=msg["data"]
                                            )
            track.append(midi_event)
        elif msg_type == MIDI_MESSAGE_CODES.TIME_SIGNATURE_EVENT:
            midi_event = midi.TimeSignatureEvent(tick=msg["tick"],
                                                 data=msg["data"]
                                                 )
            track.append(midi_event)
        elif msg_type == MIDI_MESSAGE_CODES.TEXT_META_EVENT:
            midi_event = midi.TextMetaEvent(tick=msg["tick"],
                                            text=msg["text"],
                                            data=msg["data"]
                                            )
            track.append(midi_event)

    # Now continue populating the track with MIDI events from the array
    for i in xrange(n_msg):
        # Get i-th message contents
        msg_int, tick, data0, data1 = array[i]

        if msg_int == MIDI_MESSAGE_CODES.NOTEON:
            midi_event = midi.NoteOnEvent(tick=tick,
                                          channel=0,
                                          data=[data0, data1]
                                          )
            track.append(midi_event)

        elif msg_int == MIDI_MESSAGE_CODES.NOTEOFF:
            midi_event = midi.NoteOffEvent(tick=tick,
                                           channel=0,
                                           data=[data0, data1]
                                           )
            track.append(midi_event)

        elif msg_int == MIDI_MESSAGE_CODES.AFTERTOUCH:
            midi_event = midi.AfterTouchEvent(tick=tick,
                                              channel=0,
                                              data=[data0, data1]
                                              )
            track.append(midi_event)

        elif msg_int == MIDI_MESSAGE_CODES.CONTROLCHANGE:
            midi_event = midi.ControlChangeEvent(tick=tick,
                                                 channel=0,
                                                 data=[data0, data1]
                                                 )
            track.append(midi_event)

        elif msg_int == MIDI_MESSAGE_CODES.END_OF_TRACK:
            midi_event = midi.EndOfTrackEvent(tick=tick, data=[data0, data1])
            track.append(midi_event)
            break  # Disregard anything after an End of Track event

    # Append an end-of-track event if we don't already have one.
    if type(track[-1]) is not midi.events.EndOfTrackEvent:
        eot = midi.EndOfTrackEvent(tick=1)
        track.append(eot)

    # Make sure the path to the output file is a valid directory, and create it
    # if not.
    mkpath(os.path.dirname(output_file))

    # Save the pattern to disk - note that midi_stub has now been populated.
    midi.write_midifile(output_file, midi_pattern)


if __name__ == '__main__':
    """
    Main portion of file. Note that since we're using multiprocessing, we
    *must* run a lot of code here and not in functions.
    """

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--manifestfile",
                        required=True,
                        help="Manifest file, containing one path to a midi "
                             + "file per line.")
    parser.add_argument("-o", "--outputfile",
                        required=True,
                        help="Output pickle file to create.")
    parser.add_argument("-l", "--seqlength",
                        required=False,
                        type=int,
                        default=100,
                        help="Length of sequences to use.")
    parser.add_argument("-j", "--processes",
                        required=False,
                        type=int,
                        default=4,
                        help="Number of processes to use.")
    parser.add_argument("-v", "--verbosity",
                        action="count",
                        help="increase output verbosity, e.g. -vv for "
                             + "debug level.")
    args = parser.parse_args()

    # Set the sequence length
    MAX_SEQUENCE_LENGTH = args.seqlength

    # Parse args to more convenient names
    manifest_file = args.manifestfile
    output_file = args.outputfile
    n_processes = args.processes

    # Setup logger
    logging_level = parse_logging_level(args.verbosity)
    logging.basicConfig(
            level=logging_level,
            format='[%(levelname)s] %(message)s',
            )

    # Get logger instance
    logger = logging.getLogger(__name__)

    # Get list of MIDI files
    midi_files = get_file_list_from_manifest(manifest_file)

    # Instantiate our Pool for multiprocess stuff
    pool = Pool(n_processes)

    # Array of the number of MIDI events in each file
    n_events = pool.map(get_n_events_for_file, midi_files)
    n_events = np.array(n_events)  # Convert to numpy array. Why not?

    # Log dataset statistics
    logger.info("Midi events. Total: {}. Max: {}. Avg: {}. Min: {}.".format(
        np.sum(n_events), np.max(n_events),
        np.mean(n_events), np.min(n_events)))

    parsed_out = pool.map(parse_midi_pattern, midi_files)
    # Returns a tuple for each file, the numpy.ndarray and the midi_stub dict

    midi_stubs = [x[1] for x in parsed_out]  # List of dicts
    ticks_per_quarter = [x["resolution"] for x in midi_stubs]  # List of ints

    # We have to repeat the above, since a file makes multiple sequences
    n_repeats = np.ceil(n_events/float(MAX_SEQUENCE_LENGTH)).astype(int)
    midi_stub_index = np.arange(len(midi_stubs)).repeat(n_repeats)
    ticks_per_quarter = np.array(ticks_per_quarter).repeat(n_repeats)

    # Extract and stack the parsed matrices
    Y = [x[0] for x in parsed_out]
    Y = np.vstack(Y)

    X = pool.map(quantize_parsed_midi_pattern, zip(Y, ticks_per_quarter))
    X = np.array(X)  # Convert list to numpy.ndarray

    n_train = int(round(TRAIN_FRACTION * X.shape[0]))
    n_test = X.shape[0] - n_train

    X_train = X[:n_train]
    Y_train = Y[:n_train]
    midi_stub_index_train = midi_stub_index[:n_train]

    X_test = X[n_train:]
    Y_test = Y[n_train:]
    midi_stub_index_test = midi_stub_index[n_train:]

    # Make sure the path to the output file is a valid dir, and create it
    # if not.
    mkpath(os.path.dirname(output_file))

    # Write pickled output
    with open(output_file, 'wb') as fh:
        pickle.dump(((X_train, Y_train),
                     (X_test, Y_test),
                     (midi_stubs,
                      midi_stub_index_train, midi_stub_index_test)),
                    fh
                    )
    logger.info("Wrote output to {}.".format(output_file))
    logger.info("Output format is the tuple: "
                "(X_train, Y_train), (X_test, Y_test), "
                "(midi_stubs, idx_train, idx_test)")

    # Close the multiprocessing pool
    pool.close()
