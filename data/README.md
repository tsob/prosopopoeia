# data

This folder contains data for Prosopopoeia.

## Preprocessing data for Keras input

### `make_dataset.py`

Info coming soon...

## Useful Data-related scripts

If you set up your virtual environment according to the main README, including
executing [`add_scripts_to_venv_path.sh`][add] once, [these scripts][scripts]
should already be added to your path.

### `generate_midi_manifest.sh`

Use the bash script
[`generate_midi_manifest.sh`][gen-manifest] to recursively search a directory
for MIDI files, and output a text file containing the absolute path to each
valid MIDI file, one path per line. Note that this script ignores MIDI files
whose filenames start with `._`.

Usage:
```bash
$ generate_midi_manifest.sh directory/to/search/ [outputfile.txt]
```

If the outputfile is not set, the results go to `stdout`.


### `generate_wav_from_midi.sh`

Use the bash script
[`generate_wav_from_midi.sh`][gen-wav] to render a `.wav` file from MIDI.
Requires [timidity][timidity], and works best with [FreePats][freepats].

Usage:
```bash
$ generate_wav_from_midi.sh wavfile.MID outputfile.wav
```

### `bulk_generate_wavs_from_midi.sh`

Like `generate_wav_from_midi.sh` but for many files. Use
[`bulk_generate_wavs_from_midi.sh`][gen-wavs] to recursively search a directory
for MIDI files (like with `generate_midi_manifest.sh`) and render each to a
`.wav` file in the given output directory. Note that the output directory is
flat -- the tree structure of the recursively searched input directory is
discarded. Be careful of identical filenames in different input folders.
Requires [timidity][timidity], and works best with [FreePats][freepats].

Usage:
```bash
$ bulk_generate_wavs_from_midi.sh directory/to/search/ output/directory/
```

[scripts]: https://cm-gitlab.stanford.edu/tsob/prosopopoeia/tree/master/scripts
[gen-manifest]: https://cm-gitlab.stanford.edu/tsob/prosopopoeia/tree/master/scripts/generate_midi_manifest.sh
[gen-wav]: https://cm-gitlab.stanford.edu/tsob/prosopopoeia/tree/master/scripts/generate_wav_from_midi.sh
[gen-wavs]: https://cm-gitlab.stanford.edu/tsob/prosopopoeia/tree/master/scripts/bulk_generate_wavs_from_midi.sh
[add]: https://cm-gitlab.stanford.edu/tsob/prosopopoeia/tree/master/add_scripts_to_venv_path.sh
[timidity]: https://sourceforge.net/projects/timidity/
[freepats]: http://freepats.zenvoid.org/
