"""
@name: proso.py
@desc: The Prosopopoeia project
@auth: Tim O'Brien
@date: Fall 2016
"""

import sys
import os
from distutils.dir_util import mkpath
import pickle
import time
import argparse
import logging  # TODO augment with colorama colors
import pprint
import yaml

# Keras functions to build and train our model
import keras
from keras.models import Sequential
# from keras.engine.training import slice_X
# from keras.layers import Activation, TimeDistributed
from keras.layers import RepeatVector
from keras.layers.recurrent import GRU, LSTM
from keras.layers.core import Masking
from keras.regularizers import l2
from keras.optimizers import *  # noqa
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from keras.layers.normalization import BatchNormalization

DEFAULT_CONFIG = "configs/config.template.yaml"


# Define callback to record learning rate history
class LRHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.lr = []

    def on_epoch_begin(self, epoch, logs={}):
        self.lr.append(self.model.optimizer.get_config()['lr'])


def main(dataset, config, output_dir):
    """The main function to train the Prosopopoeia net.

    Parameters
    ----------
    dataset : tuple
        The dataset tuple saved by `data/make_dataset.py` of the form
        ((X_train, Y_train),
         (X_test, Y_test),
         (midi_stubs, idx_train, idx_test))

    config : ModelConfig
        An instance of the ModelConfig class that defines hyperparameters, etc.

    output_dir : str
        The directory in which to save training stats and data.

    Returns
    -------
    None  # TODO
    """
    # Setup logger
    logger = logging.getLogger(__name__)
    logger.info("Starting Prosopopoeia!")

    # Create the output directory if necessary
    mkpath(output_dir)
    # Create the filename to use for all out output data files
    config["training"]["run_str"] = run_str = "{}_{}".format(
        time.strftime("%Y_%m_%d_%H_%M_%S"),
        os.uname()[1].replace('-', '_')
        )

    # Parse dataset contents
    train, test, (midi_stubs, idx_train, idx_test) = dataset
    X_train, Y_train = train
    X_test, Y_test = test
    # Throw out unnecessary vars
    del train, test, dataset

    # Parse dimensions, record them in config, and do some sanity checking
    config["architecture"]["n_train"] = n_train = X_train.shape[0]
    config["architecture"]["n_seq_in"] = n_seq_in = X_train.shape[1]
    config["architecture"]["n_feat"] = n_feat = X_train.shape[2]
    config["architecture"]["n_seq_out"] = n_seq_out = Y_train.shape[1]
    assert n_train == Y_train.shape[0]
    assert n_feat == Y_train.shape[2]

    logger.debug("Received {} training examples".format(n_train))
    logger.debug("with length-{} event features.".format(n_train))
    logger.debug("Max sequence lengths: {} input, {} output.".format(
        n_seq_in, n_seq_out))

    logger.info("Building model...")
    model = make_model(config)
    logger.info("Done.")

    # Save architecture as a json file
    filepath = os.path.join(
                    output_dir,
                    "{}".format(run_str),
                    "arch.json"
                    )
    mkpath(os.path.dirname(filepath))
    json_string = model.to_json()
    with open(filepath, "w") as fh:
        fh.write(json_string)
    logger.info("Wrote model architecture to {}".format(filepath))

    # Setup the callbacks

    # Record the learning rate at each epoch
    lr_history = LRHistory()

    # Checkpoint - save intermediate weights from the model
    # Formulate the output path and make sure its directory exists
    filepath = os.path.join(
                    output_dir,
                    "{}".format(run_str),
                    "weights-improvement-{epoch:02d}.hdf5"
                    )
    mkpath(os.path.dirname(filepath))
    # Extract hyperparams from config dict
    checkpoint_cfg = config["training"]["callbacks"]["checkpoint"]
    # Invoke the checkpoint
    checkpoint = ModelCheckpoint(
        filepath,
        monitor=checkpoint_cfg["monitor"],
        verbose=checkpoint_cfg["verbose"],
        save_best_only=checkpoint_cfg["save_best_only"],
        mode=checkpoint_cfg["mode"],
        )

    # Extract hyperparams from config dict
    reduce_lr_cfg = config["training"]["callbacks"]["reduce_lr_on_plateau"]
    # Create the callback
    reduce_lr_on_plateau = ReduceLROnPlateau(
        monitor=reduce_lr_cfg["monitor"],
        factor=reduce_lr_cfg["factor"],
        patience=reduce_lr_cfg["patience"],
        verbose=reduce_lr_cfg["verbose"],
        mode=reduce_lr_cfg["mode"],
        epsilon=reduce_lr_cfg["epsilon"],
        cooldown=reduce_lr_cfg["cooldown"],
        min_lr=reduce_lr_cfg["min_lr"]
        )

    # Extract hyperparams from config dict
    early_stopping_cfg = config["training"]["callbacks"]["early_stopping"]
    # Create the callback
    early_stopping = EarlyStopping(
        monitor=early_stopping_cfg["monitor"],
        min_delta=early_stopping_cfg["min_delta"],
        patience=early_stopping_cfg["patience"],
        verbose=early_stopping_cfg["verbose"],
        mode=early_stopping_cfg["mode"]
        )

    # Keep track of all our callbacks
    callbacks_list = [checkpoint, reduce_lr_on_plateau,
                      early_stopping, lr_history]

    logger.info("Starting to train.")

    # Handle inputs in the case of autencoder training.
    if config["architecture"]["autoencoder"]:
        train_input = Y_train
        train_output = Y_train
    else:
        train_input = X_train
        train_output = Y_train

    history = model.fit(
                train_input, train_output,
                batch_size=config["training"]["batch_size"],
                nb_epoch=config["training"]["n_epochs"],
                verbose=config["training"]["verbose"],
                callbacks=callbacks_list,
                validation_split=config["training"]["validation_split"],
                shuffle=config["training"]["shuffle"]
                )

    # Parse history
    stats = {}
    for key in history.history.keys():
        stats[key] = history.history[key]
    # Also save the config here
    stats["config"] = config
    # And the learning rate
    stats["learning_rate"] = lr_history.lr

    # Write out this stats dict
    with open(os.path.join(output_dir,
                           "{}.pkl".format(run_str)),
              "wb") as fh:
        pickle.dump(stats, fh)

    # Save the final model
    filepath = os.path.join(
                    output_dir,
                    "{}".format(run_str),
                    "final_model.hdf5"
                    )
    mkpath(os.path.dirname(filepath))
    model.save(filepath)

    # TODO write out the train and val outputs, so we can listen.

    # Predict on test set
    Y_pred = model.predict(
        X_test,
        batch_size=config["training"]["batch_size"],
        verbose=config["training"]["verbose"]
        )

    # Write test set outputs
    filepath = os.path.join(
                    output_dir,
                    "{}".format(run_str),
                    "test_outputs.pkl"
                    )
    mkpath(os.path.dirname(filepath))
    with open(filepath, "wb") as fh:
        pickle.dump((X_test, Y_test, Y_pred, midi_stubs, idx_test), fh)


def make_model(config):
    """Create a Keras model based on hyperparameters in the config object.

    Parameters
    ----------
    config : ModelConfig
        An instance of the ModelConfig class that defines hyperparameters, etc.

    Returns
    -------
    keras.models.Model
        The Keras model which we'll train
    """

    # Extract some params from config, for convenience
    hidden_dim = config["architecture"]["hidden_dim"]
    n_feat = config["architecture"]["n_feat"]
    n_seq_in = config["architecture"]["n_seq_in"]
    n_seq_out = config["architecture"]["n_seq_out"]
    recurrent_layer_type = config["architecture"]["recurrent_layer_type"]
    U_l2_regularizer = config["regularization"]["U_l2_regularizer"]
    W_l2_regularizer = config["regularization"]["W_l2_regularizer"]
    dropout_U = config["regularization"]["dropout_U"]
    dropout_W = config["regularization"]["dropout_W"]
    recurrent_activation = config["activations"]["recurrent"][
                                    "activation"]
    recurrent_inner_activation = config["activations"]["recurrent"][
                                    "inner_activation"]

    # Start with a sequential model
    model = Sequential()

    # Add a masking layer so we ignore zero-padding
    model.add(
        Masking(
            mask_value=0.,
            input_shape=(n_seq_in, n_feat)
        ))

    # Batch normalization since our features have very different ranges
    model.add(
        BatchNormalization(
            mode=2,
            axis=2,
        ))

    # Parse whether to use GRU or LSTM or RNN, etc.
    if recurrent_layer_type.lower() == "gru":
        MyRecurrentLayer = GRU
    elif recurrent_layer_type.lower() == "lstm":
        MyRecurrentLayer = LSTM
    else:
        logger.warning("Didn't recognize recurrent layer type. "
                       "Defaulting to GRU.")
        MyRecurrentLayer = GRU
        config["architecture"]["recurrent_layer_type"] = "GRU"

    # TODO: add bidirectional, https://keras.io/layers/wrappers/#bidirectional

    # Handle scalar layer-size case
    if type(hidden_dim) is int:
        hidden_dim = [hidden_dim]

    if len(hidden_dim) == 1:
        # Single recurrent encoding layer
        model.add(MyRecurrentLayer(
            input_shape=(None, n_feat),
            output_dim=hidden_dim[0],
            W_regularizer=l2(W_l2_regularizer),
            U_regularizer=l2(U_l2_regularizer),
            dropout_W=dropout_W,
            dropout_U=dropout_U,
            activation=recurrent_activation,
            inner_activation=recurrent_inner_activation,
            return_sequences=False,  # Should be False for last stacked layer
            name="{}_encoder_1".format(recurrent_layer_type.upper())
            ))
    else:
        # Multiple stacked recurrent encoding layers
        for i_layer in xrange(len(hidden_dim)):
            output_dim = hidden_dim[i_layer]

            if i_layer == 0:
                # First layer input
                input_shape = (None, n_feat)
            else:
                # All other layer inputs
                input_shape = (None, hidden_dim[i_layer-1])

            if i_layer == len(hidden_dim)-1:
                # Should be False for last stacked layer only
                return_sequences = False
            else:
                return_sequences = True

            model.add(MyRecurrentLayer(
                input_shape=input_shape,
                output_dim=output_dim,
                W_regularizer=l2(W_l2_regularizer),
                U_regularizer=l2(U_l2_regularizer),
                dropout_W=dropout_W,
                dropout_U=dropout_U,
                activation=recurrent_activation,
                inner_activation=recurrent_inner_activation,
                return_sequences=return_sequences,
                name="{}_encoder_{}".format(
                    recurrent_layer_type.upper(), i_layer+1)
                ))

    # Decoder part of the sequence-to-sequence model --------------------------

    # For the decoder's input, we repeat the encoded input for each time step
    model.add(RepeatVector(n_seq_out))

    if len(hidden_dim) == 1:
        # Single recurrent decoding layer
        model.add(MyRecurrentLayer(
            output_dim=n_feat,
            input_dim=hidden_dim[0],
            input_length=n_seq_out,
            W_regularizer=l2(W_l2_regularizer),
            U_regularizer=l2(U_l2_regularizer),
            dropout_W=dropout_W,
            dropout_U=dropout_U,
            activation=recurrent_activation,
            inner_activation=recurrent_inner_activation,
            return_sequences=True,  # Should always be True in the decoder side
            name="{}_decoder_1".format(recurrent_layer_type.upper())
            ))
    else:
        for i_layer in xrange(len(hidden_dim)):

            # Trace backwards through hidden sizes
            input_dim = hidden_dim[len(hidden_dim)-1-i_layer]

            if i_layer == len(hidden_dim)-1:
                output_dim = n_feat
            else:
                output_dim = hidden_dim[
                    len(hidden_dim)-2-i_layer
                    ]

            model.add(MyRecurrentLayer(
                output_dim=output_dim,
                input_dim=input_dim,
                input_length=n_seq_out,
                W_regularizer=l2(W_l2_regularizer),
                U_regularizer=l2(U_l2_regularizer),
                dropout_W=dropout_W,
                dropout_U=dropout_U,
                activation=recurrent_activation,
                inner_activation=recurrent_inner_activation,
                return_sequences=True,  # Should always be True in the decoder
                name="{}_decoder_{}".format(
                    recurrent_layer_type.upper(), i_layer+1)
                ))

    optimizer = None
    exec("optimizer = {}(lr={}, clipvalue={})".format(
            config["training"]["optimizer"],
            config["training"]["lr"],
            config["training"]["clipvalue"],
            ))

    model.compile(loss=config["training"]["loss"],
                  optimizer=optimizer,
                  metrics=config["training"]["metrics"]
                  )

    return model


def parse_logging_level(level_arg):
    """Set the logging level based on the verbosity argument.

    Parameters
    ----------
    level_arg : int
        The desired level (0, 1, 2, etc.)

    Returns
    -------
    int
        The level to give to the `logging` module.
    """
    if level_arg == 0:
        logging_level = logging.WARNING
    elif level_arg == 1:
        logging_level = logging.INFO
    elif level_arg == 2:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.DEBUG

    return logging_level


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--dataset",
                        required=True,
                        help="Dataset pkl file, as output by make_dataset.")
    parser.add_argument("-o", "--outputdir",
                        required=False,
                        default=os.path.join(".", "outputs"),
                        help="Directory to output pickled training stats.")
    parser.add_argument("-v", "--verbosity",
                        action="count",
                        help="increase output verbosity, e.g. -vv for "
                             + "debug level.")
    parser.add_argument("-c", "--config",
                        required=False,
                        default=None,
                        help="Path to a config YAML file.")
    parser.add_argument("-a", "--autoencoder",
                        action="store_true",
                        help="Whether to use an autoencoder, i.e. input and "
                             "output are the same performance MIDI")
    args = parser.parse_args()

    # Setup logger
    logging_level = parse_logging_level(args.verbosity)
    logging.basicConfig(
            level=logging_level,
            format='[%(levelname)s] %(message)s',
            )

    # Get logger instance
    logger = logging.getLogger(__name__)
    logger.info("Running {}!".format(os.path.basename(sys.argv[0])))
    logger.info("Loading dataset from {}".format(args.dataset))

    # Load in our dataset
    with open(args.dataset, 'rb') as fh:
        dataset = pickle.load(fh)

    if args.config is not None:
        # Load the specified config file
        with open(args.config, "r") as fh:
            config = yaml.load(fh)
    else:
        # Load the default config object
        with open(DEFAULT_CONFIG, "r") as fh:
            config = yaml.load(fh)

    # Allow commandline-decision about autoencoder
    config["architecture"]["autoencoder"] = args.autoencoder

    # Record the dataset in config
    if "dataset" not in config:
        config["dataset"] = {}
    config["dataset"]["name"] = os.path.basename(args.dataset)

    logger.info("Received config:\n{}".format(
        pprint.pformat(config, indent=7)))

    # Start the show
    main(dataset, config, args.outputdir)
